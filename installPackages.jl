#!/usr/local/bin/julia
#
# Installs necessary packages.
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 17-10-2018
#
# Copyright (c) 2018 Luís Moreira de Sousa. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier: EUPL-1.2
###############################################################################

using Pkg

if !in("JSON",keys(Pkg.installed()))
    Pkg.add("JSON")
end
