#!/usr/local/bin/julia
#
# Contains functions that performs coordinates transformations.
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 27-10-2018
#
# Copyright (c) 2018 Luís Moreira de Sousa. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier: EUPL-1.2
###############################################################################

function transformCoords(lonlatCoords::String, projString::Cmd)

	println("Processing element starting with: ", lonlatCoords[1:10])
	fileLonLat = "lon_lat.txt"
	fileCoords = "coords.txt"
	file = open(fileLonLat, "w")
	write(file, lonlatCoords)
	close(file)
	run(pipeline(`cs2cs +init=epsg:4326 +to $projString $fileLonLat`, stdout=fileCoords))

	jsonCoords = []
	open(fileCoords) do input
		for ln in eachline(input)
			coords = split(ln)
		    push!(jsonCoords, [parse(Float32,coords[1]), parse(Float32, coords[2])])
		end
	end
	return jsonCoords
end

function processLine(lonlatCoords::String, label::String, projString::Cmd)
	gjson = Dict()
	gjson["type"] = "Feature"
	gjson["properties"] = Dict()
	gjson["properties"]["label"] = label
	gjson["geometry"] = Dict()
	gjson["geometry"]["type"] = "LineString"
	gjson["geometry"]["coordinates"] = transformCoords(lonlatCoords, projString)
	return gjson
end

function processPolygon(lonlatCoords::String, projString::Cmd)
	gjson = Dict()
	gjson["type"] = "Feature"
	gjson["geometry"] = Dict()
	gjson["geometry"]["type"] = "Polygon"
	gjson["geometry"]["coordinates"] = []
	push!(gjson["geometry"]["coordinates"], transformCoords(lonlatCoords, projString))
	return gjson
end
