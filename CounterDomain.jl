#!/usr/local/bin/julia
#
# Produces the coordinates of a polygon that approximates the counter-domain of
# Goode's Homolosine projection. This polygon can be used to portray oceans.
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 02-10-2018
#
# Copyright (c) 2018 Luís Moreira de Sousa. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier: EUPL-1.2
###############################################################################

using JSON
include("transformCoordinates.jl")
include("Comment.jl")

projString = `+proj=igh +lat_0=0 +lon_0=0 +datum=WGS84 +units=m +no_defs`
fileGJson = "CounterDomain.geojson"

interval = 1
buffer = 1e-14
spacing = 15
interruptionNorth = -40
interruptionSouthI = -100
interruptionSouthII = -20
interruptionSouthIII = 80

segments = []
push!(segments, [-90,  90, -180])
push!(segments, [ 90,   0, interruptionNorth - buffer])
push!(segments, [  0,  90, interruptionNorth + buffer])
push!(segments, [ 90, -90, 180])
push!(segments, [-90,   0, interruptionSouthIII + buffer])
push!(segments, [  0, -90, interruptionSouthIII - buffer])
push!(segments, [-90,   0, interruptionSouthII + buffer])
push!(segments, [  0, -90, interruptionSouthII - buffer])
push!(segments, [-90,   0, interruptionSouthI + buffer])
push!(segments, [  0, -90, interruptionSouthI - buffer])

coords = ""
for segment in segments
	global coords
	step = interval
	if segment[1] > segment[2]
		step = -interval
	end

	lat = segment[1] # + step
	while lat != segment[2]
		coords = string(coords, segment[3], " ", lat, "\n")
		lat += step
	end
end
# Repeat first point to close ring
coords = string(coords, segments[1][3], " ", segments[1][1])

# -- Create GeoJSON --
main = Dict()
main["_comment"] = comment
main["type"] = "FeatureCollection"
main["features"] = []
main["crs"] = Dict()
main["crs"]["type"] = "link"
main["crs"]["properties"] = Dict()
main["crs"]["properties"]["href"] = "Homolosine.crs"
main["crs"]["properties"]["type"] = "proj4"

push!(main["features"], processPolygon(coords, projString))
output = open(fileGJson, "w")
JSON.print(output, main)
close(output)
println("All done.")
