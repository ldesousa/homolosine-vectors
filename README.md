Vectors for Goode's Homolosine projection
===============================================================================

Copyright
-------------------------------------------------------------------------------

Copyright (c) 2018 Luís Moreira de Sousa. All rights reserved.
Any use of this software constitutes full acceptance of all terms of the
document licence.

Description
-------------------------------------------------------------------------------

This project includes programmes that generates useful vector maps to work with
with Goode's Homolosine projection. At present it includes:

 - [CounterDomain](CounterDomain.jl) - creates a polygon approximation of the
 Homolosine projection counter-domain. This can be used to fix vectors wrongly
 projected by programmes that consider the counter-domain to be infinite. This
 polygon can also be used to represent the seas in global mapping.

 - [LonLatLines](LatLonLines.jl) - creates a set of meridians and parallels,
 usefull for the creation of global maps.

Outputs
-------------------------------------------------------------------------------

Vectors generated with these programmes are archived at Zenodo. Version numbers
of the vectors stored in Zenodo correspond to those tagging the source code.

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1841337.svg)](https://doi.org/10.5281/zenodo.1841337)

Licence
-------------------------------------------------------------------------------

This suite of programmes is released under the [EUPL 1.2 licence](https://joinup.ec.europa.eu/community/eupl/og_page/introduction-eupl-licence).
For full details please consult the [LICENCE](LICENCE) file.
