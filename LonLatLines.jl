#!/usr/local/bin/julia
#
# Produces longitude and latitude lines for the Homolosine projection.
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 04-10-2018
#
# Copyright (c) 2018 Luís Moreira de Sousa. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier: EUPL-1.2
################################################################################

using JSON
include("transformCoordinates.jl")
include("Comment.jl")

projString = `+proj=igh +lat_0=0 +lon_0=0 +datum=WGS84 +units=m +no_defs`
fileGJson = "ParallelsMeridians.geojson"

interruptionNorth = -40
interruptionSouthI = -100
interruptionSouthII = -20
interruptionSouthIII = 80

spacing = 15 # angular distance between parallels and meridians (degrees)
interval = 1 # angular distance between each discrete line segment (i.e. resolution)
lonStart = -180
lonEnd   =  180
latStart =  -90 + spacing
latEnd   =   90 - spacing

south_north = -90:interval:90
# This value was determined by trial and error. QGis can not take something smaller.
buffer = 1e-14

latitudeSegments = []
push!(latitudeSegments, [-180, interruptionNorth - buffer])
push!(latitudeSegments, [interruptionNorth + buffer, 180])
push!(latitudeSegments, [-180, interruptionSouthI - buffer])
push!(latitudeSegments, [interruptionSouthI   + buffer, interruptionSouthII  - buffer])
push!(latitudeSegments, [interruptionSouthII  + buffer, interruptionSouthIII - buffer])
push!(latitudeSegments, [interruptionSouthIII + buffer, 180])

main = Dict()
main["_comment"] = comment
main["type"] = "FeatureCollection"
main["features"] = []
main["crs"] = Dict()
main["crs"]["type"] = "link"
main["crs"]["properties"] = Dict()
main["crs"]["properties"]["href"] = "Homolosine.crs"
main["crs"]["properties"]["type"] = "proj4"

# -- Longitude segments --
thisMeridian = lonStart
while thisMeridian <= lonEnd
	global thisMeridian
	coords = ""
    for lat in south_north
    	coords = string(coords, thisMeridian, " ", lat, "\n")
    end
	push!(main["features"], processLine(coords, string(thisMeridian, "º"), projString))
    global thisMeridian += spacing
end

# -- Latitude segments --
thisParallel = latEnd
while thisParallel > 0
	global thisParallel
	for i in 1:1:2
		coords = string("",     latitudeSegments[i][1], " ", thisParallel, "\n")
		coords = string(coords, latitudeSegments[i][2], " ", thisParallel, "\n")
		push!(main["features"], processLine(coords, string(thisParallel, "º"), projString))
	end
    thisParallel -= spacing
end

# -- Equator --
coords = string("",    -180, " ", 0, "\n")
coords = string(coords, 180, " ", 0, "\n")
push!(main["features"], processLine(coords, "0º", projString))

thisParallel = -15
while thisParallel >= latStart
	global thisParallel
	for i in 3:1:6
		coords = string("",     latitudeSegments[i][1], " ", thisParallel, "\n")
		coords = string(coords, latitudeSegments[i][2], " ", thisParallel, "\n")
		push!(main["features"], processLine(coords, string(thisParallel, "º"), projString))
	end
    thisParallel -= spacing
end

output = open(fileGJson, "w")
JSON.print(output, main)
close(output)
println("All done.")
