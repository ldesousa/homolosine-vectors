#!/usr/local/bin/julia
#
# Comments to include in the GeoJSON files.
#
# Author: Luís de Sousa luis (dot) de (dot) sousa (at) protonmail (dot) ch
# Date: 30-10-2018
#
# Copyright (c) 2018 Luís Moreira de Sousa. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier: EUPL-1.2
###############################################################################

comment = "Copyright (c) 2018 Luís Moreira de Sousa. All rights reserved.
Any use of this document constitutes full acceptance of all terms of the
document licence.

This GeoJSON document should be accompained by a CRS definition file named
Homolosine.crs. If this file is not present it can be retrieved from the
following URL:
https://gitlab.com/ldesousa/homolosine-vectors/raw/master/Homolosine.crs

SPDX-License-Identifier: EUPL-1.2"
